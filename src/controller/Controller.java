package controller;

import java.io.InputStream;
import java.util.Optional;
import java.util.Stack;
import java.util.StringTokenizer;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.board.BoardState;
import model.player.ComputerPlayer;
import model.player.HumanPlayer;
import model.Point;
import model.player.IPlayer;
import view.View;

/**
 * Lớp điều khiển chính
 */
public class Controller {
	private View view;			// khung view
	private IPlayer player;		// Đối thủ ( người hoặc máy )
	private Stack<Point> stack; // ngăn xếp lưu các nước cờ đã đi
	private Image imageNought;		// ảnh của O
	private Image imageCross;		// ảnh của X
	private boolean isEnd;			// trận đấu kết thúc chưa ?
	private int countMove;		// đếm các nước cờ đã đi
	private String playerWin;	// người thắng cuộc

	/**
	 * Constructor
	 */
	public Controller() {
		isEnd = false;
		countMove = 0;
		playerWin = "";
		stack = new Stack<>();

		// lấy ảnh x và o
		Class<?> classImg = this.getClass();
		InputStream x = classImg.getResourceAsStream("/image/cross.png");
		InputStream o = classImg.getResourceAsStream("/image/nought.png");
		imageNought = new Image(o, 20,20, true, false);
		imageCross = new Image(x, 20, 20, true, false);
	}

    /**
     * Thuật toán xác định điểm để đánh của bot
     * @return điểm được chọn để đánh
     */
	private Point calculateBestMove() {
		return this.player.movePoint();
	}

	private int getPlayerFlag() { return player.getPlayerFlag(); }

	public void setPlayerFlag(int playerFlag) { player.setPlayerFlag(playerFlag); }

	private BoardState getBoardState() { return player.getBoardState(); }

	public boolean getIsEnd() { return isEnd; }

	/**
	 * Khi mà ấn vào một ô cờ, sẽ kích hoạt hàm này
	 * @param c nút được ấn vào
	 * @param a mảng các nút
	 */
	public void play(Button c, Button[][] a) {
		// lấy tọa độ x,y từ nút
		StringTokenizer tokenizer = new StringTokenizer(c.getAccessibleText(), ";");
		int x = Integer.parseInt(tokenizer.nextToken());
		int y = Integer.parseInt(tokenizer.nextToken());

		// Nếu đối thủ là người
		if (player instanceof HumanPlayer && BoardState.boardArr[x][y] == 0) {
			makeAMove(x, y, getPlayerFlag(), a);
		}

		if ( player instanceof ComputerPlayer && BoardState.boardArr[x][y] == 0 )
            if( makeAMove(x, y, getPlayerFlag(), a) ) {
                Point point = calculateBestMove();
                makeAMove(point.x, point.y, getPlayerFlag(), a);
            }
	}

	/**
	 * Thay đổi hình của nút được bấm vào
	 * @param x hoành độ
	 * @param y tung độ
	 * @param player người thực hiện nước đi
	 * @param arrayButtonChess mảng các ô cờ
	 */
	private void setButtonGraphic(int x, int y, int player, Button[][] arrayButtonChess) {
		arrayButtonChess[x][y].setGraphic(player == 1? new ImageView(imageCross) : new ImageView(imageNought));
		Point point = new Point(x, y, player);
		stack.push(point);
		countMove++;
	}

	/**
	 * Thay đổi hình của nút được bấm vào và đẩy điểm được bấm vào ngăn xếp các nước đã đánh
	 * @param x hoành độ
	 * @param y tung độ
	 * @param player người thực hiện nước đi
	 * @param arrayButtonChess mảng các ô cờ
     * @return true nếu ván cờ chưa kết thúc, false nếu kết thúc
	 */
	public boolean makeAMove(int x, int y, int player, Button[][] arrayButtonChess) {
		getBoardState().setPosition(x, y, player);
		setButtonGraphic(x, y, player, arrayButtonChess);

		if (getBoardState().checkEndGame(x, y) == player) {
			playerWin = player + "";
			winnerNotification("Player " + playerWin + " win!");
			return false;
		}

		// Hết ô cờ để đánh. Tuyên bố hòa.
		if (countMove == (getBoardState().height * getBoardState().width)) {
			winnerNotification("It's a draw");
			return false;
		}

		setPlayerFlag( player == 1 ? 2 : 1 );

		return true;
	}

	/**
	 * Hàm undo đưa bàn cờ trở lại trạng thái trước một nước
	 * @param arrayButtonChess mảng các quân cờ được đánh
	 */
	public void undo(Button[][] arrayButtonChess) {
		if (countMove != 1)
		for (int i = 0; i < 2; i++) if (!stack.isEmpty()) {
			// số nước đi bị trừ đi 1
			countMove--;
			// point là nước đi gần nhất
			// trong point chứa các thông tin về tọa độ nước đi và người đánh nước đi đó là x hay o
			Point point = stack.pop();
			//getBoardState();   theo lần thử sơ bộ, việc bình luận dòng này không gây ảnh hưởng

			// cập nhật trạng thái bàn cờ
			// bằng cách làm point biến mất
			BoardState.boardArr[point.x][point.y] = 0;
			arrayButtonChess[point.x][point.y].setGraphic(null);
		}
	}

	public void setPlayer(IPlayer player) { this.player = player; }

	/**
	 * Cảnh báo hiện lên khi một ván chơi kết thúc
	 * @param title Người thắng cuộc là ...
	 */
	private void winnerNotification(String title) {
		isEnd = true;

		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Game Over");
		alert.setHeaderText(title);
		alert.setContentText("Try Again?");

		Optional<ButtonType> result = alert.showAndWait();
		result.ifPresent(i -> {
			if (result.get() == ButtonType.OK) {
			    view.newMatch(getPlayer());
			} else {
				// Kích hoạt khi chọn Cancel hoặc đóng hội thoại
				System.exit(0);
			}
		});

	}

	public void setView(View view) { this.view = view; }

	public void setIsEnd(boolean isEnd) { this.isEnd = isEnd; }

	private IPlayer getPlayer() { return player; }

	/**
	 * Đưa bàn cờ về trạng thái ban đầu
	 * @param arrayButtonChess Mảng các nút trên bàn cờ
	 */
	public void reset(Button[][] arrayButtonChess) {
		// Tổng nước đi bằng 0
		countMove = 0;
		// Đưa trạng thái của các ô cờ về trạng thái ban đầu
		getBoardState().resetBoard();
		// xóa Graphic của các ô cờ
		for (Button[] buttonChess : arrayButtonChess) {
			for (Button chess : buttonChess) {
				chess.setGraphic(null);
			}
		}
	}

}