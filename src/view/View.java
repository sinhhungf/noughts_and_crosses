package view;

import java.io.InputStream;
import java.util.Optional;

import controller.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import model.board.BoardState;
import model.player.ComputerPlayer;
import model.player.HumanPlayer;
import model.player.IPlayer;

/**
 * GUI
 */
public class View implements EventHandler<ActionEvent> {
	private static final int WIDTH_BOARD = 18;      // Số ô cờ theo chiều ngang
	private static final int HEIGHT_BOARD = 18;     // Số ô cờ theo chiều dọc
	private static final int WIDTH_PANE = 800;     // Chiều ngang của bàn cờ
	private static final int HEIGHT_PANE = 1000;     // Chiều cao của bàn cờ
    private static final int WIDTH_MENU_ITEM = 30;  // Chiều ngang một nút trên thanh menu
    private static final int HEIGHT_MENU_ITEM = 30; // Chiều dọc một nút trên thanh menu

    private Button btnTwoPlayersMode;               // chế độ 2 người chơi
	private Button btnBotMode;                      // chế độ chơi với máy
	private Button btnExit;                         // nút Thoát
	private Button btnUndo;                         // nút Đi lại

    private Controller controller;                  // lớp điều khiển
    private Button[][] arrayButtonChess;            // mảng quân cờ khi đánh
	private static Stage primaryStage;               // khung view

    /**
     * Constructor
     */
    public View(Stage primaryStage) { start(primaryStage); }

    /**
     * Khởi tạo màn hình chơi và khởi tạo controller
     * @param primaryStage sân khấu chính
     */
	private void start(Stage primaryStage) {
		try {
			View.primaryStage = primaryStage;
			arrayButtonChess = new Button[WIDTH_BOARD][HEIGHT_BOARD];
            BoardState boardState = new BoardState(WIDTH_BOARD, HEIGHT_BOARD);

            // con bot
            ComputerPlayer computer = new ComputerPlayer(boardState);
			controller = new Controller();
			controller.setView(this);
			controller.setPlayer(computer);
			// Mặc định người chơi 1 đi trước
			controller.setPlayerFlag(1);

			BorderPane borderPane = new BorderPane();
			BorderPane borderPaneTop = new BorderPane();
			createMenuBar(borderPaneTop);
			
			GridPane root = new GridPane();
			Scene scene = new Scene(borderPane, WIDTH_PANE, HEIGHT_PANE);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			borderPane.setPadding(new Insets(15));
			borderPane.setCenter(root);
			borderPane.setTop(borderPaneTop);

			for (int i = 0; i < WIDTH_BOARD; i++) {
				for (int j = 0; j < HEIGHT_BOARD; j++) {
					Button button = new Button();
					button.setPrefSize(40, 40);
					button.setAccessibleText(i + ";" + j);
					arrayButtonChess[i][j] = button;
					root.add(button, j, i);
                    button.setOnAction( event -> {
                        if (!controller.getIsEnd())
                            controller.play(button, arrayButtonChess); }
                    );
				}
			}
			primaryStage.setScene(scene);
			primaryStage.setTitle("Noughts and Crosses");
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Tạo thanh createMenuBar
	 * @param pane BorderPane chứa thanh createMenuBar
	 */
	private void createMenuBar(BorderPane pane) {
		HBox box = new HBox();
		box.setSpacing(50);
		Class<?> clazz = this.getClass();
		AnchorPane anchorPaneLogo = new AnchorPane();
		HBox hBox = new HBox();
		hBox.setSpacing(20);

		hBox.setAlignment(Pos.CENTER);
		// set logo
        InputStream input = clazz.getResourceAsStream("/image/logo.jpg");
		Image image = new Image(input);
		ImageView imgView = new ImageView(image);
		imgView.setFitHeight(60);
		imgView.setFitWidth(60);
		anchorPaneLogo.getChildren().add(imgView);

		btnBotMode = addMenuButton("/image/bot.png");		//chế độ chơi với máy
		btnTwoPlayersMode = addMenuButton("/image/human.png"); //chế độ chơi với người
		btnUndo = addMenuButton("/image/rewind.jpg"); // Undo
		btnExit = addMenuButton("/image/exit.jpg");  // Exit

//		Thêm các nút vào hBox; thêm logo và hBox vào box
		hBox.getChildren().addAll(btnBotMode, btnTwoPlayersMode, btnUndo, btnExit);
		box.getChildren().addAll(anchorPaneLogo, hBox);

		pane.setCenter(box); // Đặt box vào center
	}

    /**
     * Thêm nút vào thanh Menu
     * @param imageLink đường dẫn tới tập tin hình ảnh
     * @return nút để thêm vào thanh Menu
     */
	private Button addMenuButton(String imageLink) {
		Button btn = new Button();
		Image img = new Image(getClass().getResourceAsStream(imageLink));
		ImageView imgView = new ImageView(img);
		imgView.setFitHeight(HEIGHT_MENU_ITEM);
		imgView.setFitWidth(WIDTH_MENU_ITEM);
		btn.setGraphic(imgView);
		btn.setId("btnMenu");
		btn.setOnAction(this);
		return btn;
	}

    /**
     * Hàm handle xử lý các sự kiện xảy ra khi bấm nút
     * @param e sự kiện được kích hoạt
     */
	@Override
	public void handle(ActionEvent e) {
		// Tắt trò chơi
	    if (e.getSource() == btnExit) {
			primaryStage.close();
		}

	    // Bắt đầu ván chơi mới
		if (e.getSource() == btnTwoPlayersMode)
			newMatch(new HumanPlayer(new BoardState(WIDTH_BOARD, HEIGHT_BOARD)));
		if (e.getSource() == btnBotMode)
			newMatch(new ComputerPlayer(new BoardState(WIDTH_BOARD, HEIGHT_BOARD)));

        // Quay lại một bước
		if (e.getSource() == btnUndo) {
			controller.undo(arrayButtonChess);
		}
	}

    /**
     * xóa hết các ô cờ và bắt đầu một ván chơi mới
     * @param opponent đối thủ
     */
	public void newMatch(IPlayer opponent) {
	    controller.setIsEnd(false);
	    controller.setPlayer(opponent);
	    controller.setPlayerFlag(1);
	    controller.reset(arrayButtonChess);
	    if ( opponent instanceof ComputerPlayer ) selectTurn();
    }

    /**
     * Xét xem ai đi trước
     */
    private void selectTurn() {
		Alert al = new Alert(AlertType.CONFIRMATION);
		al.setTitle("Select Turn");
		al.setHeaderText("Do you want to go first ?");

		Optional<ButtonType> result = al.showAndWait();

		result.ifPresent(i -> {
            if(result.get() == ButtonType.CANCEL)
                controller.makeAMove(WIDTH_BOARD/2 - 1, HEIGHT_BOARD/2,1, arrayButtonChess); }
                );
	}
}