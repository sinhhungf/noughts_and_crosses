package model;

/**
 *  Tọa độ mỗi quân cờ
 */
public class Point {
	public int x;		// hoành độ của quân cờ
	public int y;		// tung độ của quân cờ
	private int player;	// người đánh quân cờ

	public Point() {
	}

	public Point(int x, int y, int player) {
		this.x = x;
		this.y = y;
		this.player = player;
	}

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return x + ";" + y + ";" + player;
	}

}
