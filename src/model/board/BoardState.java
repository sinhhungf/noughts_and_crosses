package model.board;

/**
 *  Lớp lưu lại trạng thái của bàn cờ
 */
public class BoardState {
	// Mảng lưu lại trạng thái của các ô cờ
	public static int[][] boardArr;

	public int width;		// chiều rộng của bàn cờ
	public int height;		// chiều cao của bàn cờ

	public BoardState(int width, int height) {
		boardArr = new int[width][height];
		this.height = height;
		this.width = width;
	}

	/**
	 * Đưa trạng thái của các ô cờ về trạng thái ban đầu
	 */
	public void resetBoard(){
		boardArr = new int[width][height];
	}

	/**
	 * Kiểm tra xem ván đấu đã kết thúc chưa và ai là người chiến thắng
	 * @param row tọa độ x
	 * @param col tọa độ y
	 * @return 0 - chưa ai chiến thắng; 1,2 - người chơi 1 ( 2 ) chiến thắng
	 */
	public int checkEndGame(int row, int col) {
		int r = 0, c = 0;
		int i;
		boolean human, pc;

		// Hàng ngang
		while (c < width - 4) {
			human = pc = true;

			for (i = 0; i < 5; i++) {
				if (boardArr[row][c + i] != 1) human = false;
				if (boardArr[row][c + i] != 2) pc = false;
			}

			if (human) return 1;
			if (pc) return 2;

			c++;
		}

		// Hàng dọc
		while (r < height - 4) {
			human = pc = true;

			for (i = 0; i < 5; i++) {
				if (boardArr[r + i][col] != 1) human = false;
				if (boardArr[r + i][col] != 2) pc = false;
			}

			if (human) return 1;
			if (pc) return 2;

			r++;
		}

		// Chéo xuống ( từ (0,0) đến (n,n) )
		r = row;
		c = col;
		while (r > 0 && c > 0) {
			r--;
			c--;
		}
		while (r < height - 4 && c < width - 4) {
			human = pc = true;

			for (i = 0; i < 5; i++) {
				if (boardArr[r + i][c + i] != 1) human = false;
				if (boardArr[r + i][c + i] != 2) pc = false;
			}
			if (human) return 1;
			if (pc) return 2;

			r++;
			c++;
		}

		// Chéo lên ( từ (0,n) đến (n,0) )
		r = row;
		c = col;
		while (r < height - 1 && c > 0) {
			r++;
			c--;
		}

		while (r >= 4 && c < height - 4) {
			human = pc = true;

			for (i = 0; i < 5; i++) {
				if (boardArr[r - i][c + i] != 1) human = false;
				if (boardArr[r - i][c + i] != 2) pc = false;
			}

			if (human) return 1;
			if (pc) return 2;

			r--;
			c++;
		}

		return 0;
	}

	public int getPosition(int x, int y) {
		return boardArr[x][y];
	}

	public void setPosition(int x, int y, int player) {
		boardArr[x][y] = player;
	}
}
