package model.board;

import model.Point;

/**
 *  Lớp đánh giá trạng thái cho mỗi ô cờ
 */
public class BoardEvaluation {
	public int height, width;
	public int[][] EBoard;
	public int maxEvaluation = 0;


	public BoardEvaluation(int height, int width) {
		this.height = height;
		this.width = width;
		EBoard = new int[height][width];
	}

	public void resetBoard() {
		for (int r = 0; r < height; r++)
			for (int c = 0; c < width; c++)
				EBoard[r][c] = 0;
	}

	public Point getMaxPosition() {
		int max = 0; // diem max
		Point p = new Point();
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (EBoard[i][j] > max) {
					p.x = i;
					p.y = j;
					max = EBoard[i][j];
				}
			}
		}
		if (max == 0) {
			return null;
		}
		maxEvaluation = max;
		return p;
	}

}
