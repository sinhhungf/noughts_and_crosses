package model.player;

import model.board.BoardState;
import model.Point;

/**
 *  giao diện chung của Computer Player và Human Player
 */
public interface IPlayer {
	Point movePoint();

	int getPlayerFlag();

	void setPlayerFlag(int playerFlag);

	BoardState getBoardState();
}
