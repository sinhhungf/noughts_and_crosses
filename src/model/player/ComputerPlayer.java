package model.player;

import model.board.BoardState;
import model.board.BoardEvaluation;
import model.Point;

import java.util.ArrayList;

/**
 *  Lớp ComputerPlayer con bot
 */

public class ComputerPlayer implements IPlayer {
	private BoardEvaluation eBoard;        // điểm của trạng thái bàn cờ
	private BoardState boardState;   // trạng thái bàn cờ
	private int playerFlag = 2;      // Đánh dấu là Computer Player
	private int _x, _y;              // Tọa độ nước đi

	private static int maxDepth = 6;            // do sau toi da
	private static int maxMove = 4;             // so o tiep theo dem xet toi da
	
	private int[] AScore = {0,4,27,256,1458};   // Mảng điểm tấn công 0,4,28,256,2308
	private int[] DScore = {0,2,9,99,769};      // Mảng điểm phòng ngự 0,1,9,85,769

	private Point goPoint;

	public ComputerPlayer(BoardState board) {
		this.boardState = board;
		this.eBoard = new BoardEvaluation(board.height, board.width);
	}

	// ham luong gia
	private void evaluateBoard(BoardEvaluation eBoard) {
//	    int player = 2;
		int row, col;
		int ePC, eHuman;
		eBoard.resetBoard(); // Đặt lại tất cả điểm trạng thái của các ô cờ

		// Duyet theo hang
		for (row = 0; row < eBoard.width; row++)
			for (col = 0; col < eBoard.height - 4; col++) {
				ePC = 0;
				eHuman = 0;
				for (int i = 0; i < 5; i++) {
					if (boardState.getPosition(row, col + i) == 1) // neu quan do la cua human
						eHuman++;
					if (boardState.getPosition(row, col + i) == 2) // neu quan do la cua pc
						ePC++;
				}
				// trong vong 5 o khong co quan dich
				if (eHuman * ePC == 0 && eHuman != ePC)
					for (int i = 0; i < 5; i++) {
						if (boardState.getPosition(row, col + i) == 0) { // neu o chua danh
							if (eHuman == 0) // ePC khac 0
									eBoard.EBoard[row][col + i] += AScore[ePC];// cho diem tan cong
							if (ePC == 0) // eHuman khac 0
									eBoard.EBoard[row][col + i] += DScore[eHuman];// cho diem phong ngu	
							if (eHuman == 4 || ePC == 4)
								eBoard.EBoard[row][col + i] *= 2;
						}
					}
			}

		// Duyet theo cot
		for (col = 0; col < eBoard.height; col++)
			for (row = 0; row < eBoard.width - 4; row++) {
				ePC = 0;
				eHuman = 0;
				for (int i = 0; i < 5; i++) {
					if (boardState.getPosition(row + i, col) == 1)
						eHuman++;
					if (boardState.getPosition(row + i, col) == 2)
						ePC++;
				}
				if (eHuman * ePC == 0 && eHuman != ePC)
					for (int i = 0; i < 5; i++) {
						if (boardState.getPosition(row + i, col) == 0) // Neu o chua duoc danh
						{
							if (eHuman == 0)
									eBoard.EBoard[row + i][col] += AScore[ePC];
							if (ePC == 0)
									eBoard.EBoard[row + i][col] += DScore[eHuman];
							if (eHuman == 4 || ePC == 4)
								eBoard.EBoard[row + i][col] *= 2;
						}

					}
			}

		// Duyet theo duong cheo xuong
		for (col = 0; col < eBoard.height - 4; col++)
			for (row = 0; row < eBoard.width - 4; row++) {
				ePC = 0;
				eHuman = 0;
				for (int i = 0; i < 5; i++) {
					if (boardState.getPosition(row + i, col + i) == 1)
						eHuman++;
					if (boardState.getPosition(row + i, col + i) == 2)
						ePC++;
				}
				if (eHuman * ePC == 0 && eHuman != ePC)
					for (int i = 0; i < 5; i++) {
						if (boardState.getPosition(row + i, col + i) == 0) // Neu o chua duoc danh
						{
							if (eHuman == 0)
									eBoard.EBoard[row + i][col + i] += AScore[ePC];
							if (ePC == 0)
									eBoard.EBoard[row + i][col + i] += DScore[eHuman];
							if (eHuman == 4 || ePC == 4)
								eBoard.EBoard[row + i][col + i] *= 2;
						}

					}
			}

		// Duyet theo duong cheo len
		for (row = 4; row < eBoard.width; row++)
			for (col = 0; col < eBoard.height - 4; col++) {
				ePC = 0; // so quan PC
				eHuman = 0; // so quan Human
				for (int i = 0; i < 5; i++) {
					if (boardState.getPosition(row - i, col + i) == 1) // neu la human
						eHuman++; // tang so quan human
					if (boardState.getPosition(row - i, col + i) == 2) // neu la PC
						ePC++; // tang so quan PC
				}
				if (eHuman * ePC == 0 && eHuman != ePC)
					for (int i = 0; i < 5; i++) {
						if (boardState.getPosition(row - i, col + i) == 0) { // neu o chua duoc danh
							if (eHuman == 0)
									eBoard.EBoard[row - i][col + i] += AScore[ePC];
							if (ePC == 0)
									eBoard.EBoard[row - i][col + i] += DScore[eHuman];
							if (eHuman == 4 || ePC == 4)
								eBoard.EBoard[row - i][col + i] *= 2;
						}
					}
			}

	}

//	/**
//	 * Thuật toán Alpha-Beta để con bot đưa ra quyết định đánh cờ
//	 * @param alpha alpha
//	 * @param beta beta
//	 * @param depth độ sâu
// 	 * @param player người chơi
//	 */
//	private void alphaBeta(int alpha, int beta, int depth, int player) {
//		maxValue(boardState, alpha, beta, depth);
//		//		if ( player == 2 ){
////			maxValue(boardState, alpha, beta, depth);
////		} else {
////			minValue(boardState, alpha, beta, depth);
////		}
//	}

	private int maxValue(BoardState state, int alpha, int beta, int depth) {
		eBoard.getMaxPosition();                    // Tọa độ có điểm cao nhất
		int value = eBoard.maxEvaluation; 			// Giá trị max hiện tại

        if (depth >= maxDepth) {
			return value;
		}

		evaluateBoard( eBoard ); // danh gia diem voi nguoi choi hien tai la PC

		ArrayList<Point> list = new ArrayList<>(); // list cac nut con
		for (int i = 0; i < maxMove; i++) {
			Point node = eBoard.getMaxPosition();
			if(node == null)
				break;
			list.add(node);
			eBoard.EBoard[node.x][node.y] = 0;
		}

		int v = Integer.MIN_VALUE;
		for (Point com : list) {
			state.setPosition(com.x, com.y, 2);
			v = Math.max(v, minValue(state, alpha, beta, depth + 1));
			state.setPosition(com.x, com.y, 0);
			if (v >= beta || state.checkEndGame(com.x, com.y) == 2) {
				goPoint = com;
				return v;

			}
			alpha = Math.max(alpha, v);
		}

		return v;
	}

	private int minValue(BoardState state, int alpha, int beta, int depth) {
		
		eBoard.getMaxPosition();
		int value = eBoard.maxEvaluation;
		if (depth >= maxDepth) {
			return value;
		}
		
		ArrayList<Point> list = new ArrayList<>(); // list cac nut con 
		for (int i = 0; i < maxMove; i++) {
			Point node = eBoard.getMaxPosition();
			if(node==null)
				break;
			list.add(node);
			eBoard.EBoard[node.x][node.y] = 0;
		}
		int v = Integer.MAX_VALUE;
		for (Point com : list) {
			state.setPosition(com.x, com.y, 1);
			v = Math.min(v, maxValue(state, alpha, beta, depth + 1));
			state.setPosition(com.x, com.y, 0);
			if (v <= alpha || state.checkEndGame(com.x, com.y) == 1) {
				return v;
			}
			beta = Math.min(beta, v);
		}
		return v;
	}

	// Tính toán nước đi
	private Point calculateBestMove() {
//		alphaBeta(0, 1,2 ,player);
		maxValue( boardState, 0, 1, 2 );
		Point temp = goPoint;
		if (temp != null) {
			_x = temp.x;
			_y = temp.y;
		}
		return new Point(_x, _y);
	}

	@Override
	public int getPlayerFlag() {
		return playerFlag;
	}

	@Override
	public void setPlayerFlag(int playerFlag) {
		this.playerFlag = playerFlag;
	}

	@Override
	public BoardState getBoardState() {
		return boardState;
	}

	@Override
	public Point movePoint() {
		return calculateBestMove();
	}

}
