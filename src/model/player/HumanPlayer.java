package model.player;

import model.board.BoardState;
import model.Point;

public class HumanPlayer implements IPlayer {
	private BoardState boardState; 	// Bàn cờ để xử lý
	private int playerFlag = 1; 	// Đánh đầu là người

	public HumanPlayer(BoardState board) {
		this.boardState = board;
	}

	@Override
	public Point movePoint() {
		return null;
	}

	@Override
	public int getPlayerFlag() {
		return playerFlag;
	}

	@Override
	public void setPlayerFlag(int playerFlag) {
		this.playerFlag = playerFlag;
	}

	@Override
	public BoardState getBoardState() {
		return boardState;
	}
}
