package application;

import javafx.application.Application;
import javafx.stage.Stage;
import view.View;

/**
 *  Lớp khởi chạy ứng dụng
 */
public class Main extends Application {
	// initialize stage
	@Override
	public void start(Stage primaryStage) { new View(primaryStage); }

	public static void main(String[] args) {
		launch(args);
	}
}